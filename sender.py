#!/usr/bin/python3

import asyncio, configparser, ipaddress, argparse, trace, logging, os
import socket

from scapy.all import DNS, DNSQR, IP, IPv6, sr, UDP, send, conf, ICMP, sendp, Ether


class Sender:

    def __init__(self, option, infile):
        parser = configparser.ConfigParser()
        parser.read("/etc/dnsaal/conf.d/config")

        self.port = parser.get("dnsaal", "port")
        self.ip = parser.get("dnsaal", "ip")
        self.ip6 = parser.get("dnsaal", "ip6")
        self.interface = parser.get("sender", "interface")

        self.path = parser.get("files", "path")
        self.csv = infile
        #self.csv = parser.get("files", "csv")

        self.qname = parser.get("dnsaal", "query_name")
        self.log_file = parser.get("sender", "log_file")
        self.logger = self._init_logger()

        self.socket = self.create_socket()
        self.option = option

    def _init_logger(self):
        logging.basicConfig(filename=self.log_file, filemode='a+', format="%(levelname)8s | %(asctime)s | %(message)s",
                            datefmt='%Y-%m-%d %H:%M:%S')
        logger = logging.getLogger(__name__)
        logger.propagate = False
        logger.setLevel(logging.DEBUG)
        #stdout_handler = logging.StreamHandler()
        #logger.addHandler(stdout_handler)
        return logger

    def trace(self):
        pass

    def read_file(self):
        with open(self.path+'/'+self.csv, mode='r') as csv:
            file = csv.read()
            print(file)

    def create_socket(self):
        return conf.L3socket(iface=self.interface)

    async def query(self, dst_address, qname):
        try:
            if ipaddress.IPv6Address(dst_address):
                self.logger.info(f"Send IPv6 query to {dst_address}")
                request = IPv6(src=self.ip6, dst=dst_address) / \
                          UDP(sport=int(self.port)) / \
                          DNS(qd=DNSQR(qname=qname))
                self.socket.send(request)
        except ValueError:
            pass
        try:
            if ipaddress.IPv4Address(dst_address):
                self.logger.info(f"Send IPv4 query to {dst_address}")
                request = IP(src=self.ip, dst=dst_address) / \
                          UDP(sport=int(self.port)) / \
                          DNS(qd=DNSQR(qname=qname))
                self.socket.send(request)
        except ValueError:
            pass

    async def testing(self):
        pass

    async def send_queries(self):
        with open(self.path+'/'+self.csv, mode='r') as file:
            #addresses = [self.query(row.split(',')[1].strip(), self.qname) for row in file]
            addresses = [self.query(row.strip(), self.qname) for row in file]
            await asyncio.gather(*addresses)

    async def resolve_authoritative(self):
        pass

    async def start(self):
        if self.option == "normal":
            await self.send_queries()
        if self.option == "testing":
            await self.testing()
        else:
            pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='DNS-RPK-WHY Query Sender',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("option",
                        choices=['normal', 'testing'],
                        help="Type of operation",
                        type=str)
    parser.add_argument("input",
                        help='input file',
                        type=str)
    sender = Sender(parser.parse_args().option, parser.parse_args().input)
    asyncio.get_event_loop().run_until_complete(sender.start())
