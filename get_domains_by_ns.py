#!/usr/bin/python3
import argparse
import csv
import os
from datetime import datetime

import dns.resolver
import dns.reversename
import numpy
import pandas as pd


class GetDomainsByNS:

    def __init__(self, **kwargs):

        parser = argparse.ArgumentParser(
            description="This tool gets the DNS name for an IP address and then counts all domains it serves."
        )
        parser.add_argument('input_file', help="File with IP addresses, one per line.")
        parser.add_argument('output_file', help="File where output will be written to in CSV format.")
        parser.add_argument('--use_dns', action='store_true',
                            help="When this flag is present, a reverse DNS lookup will be done to determine the DNS "
                                 "name associated with the IP address. Can not be used in conjunction with "
                                 "--ns_ip_list_file.")
        parser.add_argument('--zone_file', type=str,
                            help="A complete zone file can be supplied. This will then be used to count all the "
                                 "domains each NS serves. Can not be used in conjunction with --ns_names_counts.")
        parser.add_argument('--ns_ip_list_file', type=str,
                            help="File based reverse DNS lookup. CSV format: NS followed by IP address. Can not be "
                                 "used in conjunction with --use_dns")
        parser.add_argument('--ns_names_counts', type=str,
                            help="This file will be used to get the NS names count. Alternative to the zone file. CSV "
                                 "format: NS followed by count. Can not be used in conjunction with --zone_file.")
        self.args = parser.parse_args()

        if self.args.zone_file is not None and self.args.ns_names_counts is not None:
            print("Please only use zone_file or ns_names_counts, not both")
            exit(1)

        if self.args.use_dns is True and self.args.ns_ip_list_file is not None:
            print("Please only use use_dns or ns_ip_list_file, not both")
            exit(1)

        if self.args.ns_ip_list_file:
            self.ns_ip_list = pd.read_csv(self.args.ns_ip_list_file, header=None)
            self.ns_ip_list = self.ns_ip_list.to_numpy()

        if self.args.ns_names_counts:
            self.ns_names_counts = pd.read_csv(self.args.ns_names_counts, header=None)
            self.ns_names_counts = self.ns_names_counts.to_numpy()

    def find_ns_by_reverse_dns(self, ip_address):
        ptr = dns.reversename.from_address(ip_address)
        try:
            name_server = dns.resolver.resolve(str(ptr), "PTR")[0]
            return name_server
        except dns.exception.Timeout:
            return 1

    def find_domains_associated(self, name_server):

        if self.args.zone_file is not None:
            command = f'grep -w "{name_server}" {self.args.zone_file} | wc -l'
            # command = f"awk -F ' ' ' '$5~/^{name_server}$/{{count++}} END{{print count}}' {self.args.zone_file}"
            domains = os.popen(command).read()
            if not domains:
                domains = 0
            else:
                domains = domains.strip('\n')
        else:
            # command = f"awk -F ',' '$1~/^{name_server}$/{{print $2}}' {self.args.ns_names_counts}"
            # command = f'grep "^{name_server}," {self.args.ns_names_counts}'
            # domains = os.popen(command).read().strip('\n')
            # if not domains:
            #     domains = 0
            # else:
            #     domains = domains.split(',')[1].strip('\n')
            domains = self.name_server_counts_from_files(name_server)
        try:
            domains = int(domains)
        except ValueError:
            domains = -1
        return domains

    def name_server_from_file(self, ip_address):
        index = numpy.where(self.ns_ip_list[:, 1] == ip_address)
        name_server = self.ns_ip_list[index]
        if name_server.size == 0:
            return 1
        return name_server[0][0]

    def name_server_counts_from_files(self, name_server):
        index = numpy.where(self.ns_names_counts[:, 0] == name_server)
        count = self.ns_names_counts[index]
        if count.size == 0:
            count = 0
        else:
            count = count[0][1]
        return count

    def start(self):

        print(f'Start associating: {datetime.now()}')
        with open(self.args.input_file) as input_file:

            with open(self.args.output_file, mode='a+', newline='') as output_file:
                writer = csv.writer(output_file)
                already_processed = None
                if os.stat(self.args.output_file).st_size != 0:
                    output_df = pd.read_csv(self.args.output_file, header=None)
                    already_processed = set(output_df.iloc[:, 0].values)

                for line in input_file:
                    ip_address = str(line).strip()
                    if self.args.use_dns is True:
                        name_server = str(self.find_ns_by_reverse_dns(ip_address))
                        if name_server == 1:
                            # print(f'Timeout for {ip_address}')
                            continue
                    else:
                        # command = f'grep "{ip_address}" {self.args.ns_ip_list_file}'
                        # name_server = os.popen(command).read()
                        # if name_server == '':
                        #     print(f'No DNS name found for {ip_address}')
                        #     continue
                        # name_server = name_server.split(',')[0]

                        name_server = self.name_server_from_file(ip_address)
                        if name_server == 1:
                            # print(f'No DNS name found for {ip_address}')
                            continue

                        if already_processed is not None and name_server in already_processed:
                            # print(f'{name_server} is already in the output file')
                            continue

                    domains = self.find_domains_associated(name_server)
                    if domains == -1:
                        # print(f'{name_server} could not be associated for some reason')
                        continue

                    # print(f'{name_server} serves {domains} domains')
                    writer.writerow([
                        name_server,
                        domains
                    ])

        print(f'Finished associating: {datetime.now()}')


if __name__ == '__main__':
    service = GetDomainsByNS()
    service.start()
