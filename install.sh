#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# Add user if it does not exist
printf "Adding user dnsaal\n"
id -u dnsaal >/dev/null 2>&1 || sudo useradd -r -s /usr/sbin/nologin -b /etc/dnsaal/ dnsaal

# Create directories
printf "Creating /etc/dnsaal\n"
mkdir -p /etc/dnsaal/conf.d/
cp config /etc/dnsaal/conf.d/
printf "Copying to /etc/dnsaal/dns_aa_listener.py\n"
cp dns_aa_listener.py /etc/dnsaal/
printf "Creating config file\n"
touch /etc/dnsaal/conf.d/config

printf "Chowning /etc/dnsaal/\n"
chown -R dnsaal:dnsaal /etc/dnsaal/
printf "Chmodding\n"
chmod 0775 /etc/dnsaal/

printf "Creating /var/log/dnsaal.log\n"
touch /var/log/dnsaal.log
printf "Chowning\n"
chown dnsaal:dnsaal /var/log/dnsaal.log
printf "Chmodding\n"
chmod 0644 /var/log/dnsaal.log

printf "Copying service file to /etc/systemd/system/dnsaal.service\n"
cp dnsaal.service /etc/systemd/system/
printf "Chowning\n"
chown root:root /etc/systemd/system/dnsaal.service
printf "Chmodding\n"
chmod 0644 /etc/systemd/system/dnsaal.service

printf "Done\n"