import configparser
import csv
import errno
import logging
import signal
import socket
import datetime
import time
from ipaddress import IPv6Address


class DNSAuthoritativeAnswerListener:

    def __init__(self):
        signal.signal(signal.SIGTERM, self._handle_sigterm)

        parser = configparser.ConfigParser()
        parser.read("/etc/dnsaal/conf.d/config")

        self.port = parser.get("dnsaal", "port")
        self.ip = parser.get("dnsaal", "ip")
        self.ip6 = parser.get("dnsaal", "ip6")
        self.query_name = parser.get('dnsaal', 'query_name')
        self.query_name_length = len(self.query_name.encode('utf-8'))
        self.directory = parser.get('files', 'path')
        self.output = parser.get('dnsaal', 'output')
        self.log_file = parser.get('dnsaal', 'log_file')

        self.buffer_size = 1024

        self.logger = self._init_logger()
        self.logger.info("DNSAuthoritativeAnswerListener instance created")

        self.udp_server_socket = socket.socket(family=socket.AF_INET6, type=socket.SOCK_DGRAM)

    def _init_logger(self):
        logging.basicConfig(filename=self.log_file, filemode='a+', format="%(levelname)8s | %(asctime)s | %(message)s",
                            datefmt='%Y-%m-%d %H:%M:%S')

        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        stdout_handler = logging.StreamHandler()
        logger.addHandler(stdout_handler)
        return logger

    def _handle_sigterm(self, sig, frame):
        self.logger.warning('SIGTERM received...')
        self.stop()

    @staticmethod
    def fix_ipaddress(address):
        try:
            if IPv6Address(address).ipv4_mapped:
                return format(IPv6Address(address).ipv4_mapped)
            else:
                return address
        except ValueError:
            pass

    def start(self):

        # file_name = self.directory + '/' + self.output
        #  Start listening UDP/Datagram socket

        self.udp_server_socket.bind(('::', int(self.port)))
        self.logger.info(f'Listening on :::{self.port}')

        while True:
            try:
                data, addr = self.udp_server_socket.recvfrom(self.buffer_size)
            except OSError as e:
                if e.errno == errno.EBADF:
                    break
                else:
                    raise

            query_name = data[13:13 + self.query_name_length]
            query_name = query_name.replace(b'\x03', b'\x2E')
            query_name = query_name.decode()

            addr_ip = self.fix_ipaddress(addr[0])
            if query_name == self.query_name:
                with open(self.directory + "/" + "{}_".format(datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%Y_%H')) + self.output, 'a+') as f:
                    writer = csv.writer(f)
                    writer.writerow([
                        datetime.datetime.now(),
                        query_name,
                        addr_ip
                    ])
                self.logger.info(f'Query name {query_name} from {addr_ip}')

    def stop(self):
        self.logger.info(f'Closing listener on {self.ip}:{self.port}')
        self.udp_server_socket.close()


if __name__ == '__main__':
    service = DNSAuthoritativeAnswerListener()
    service.start()
