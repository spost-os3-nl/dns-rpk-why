#!/usr/bin/python3

import ipaddress, os, glob, configparser, argparse, json, logging, multiprocessing
from datetime import datetime, timedelta


'''
With this analyzing class all gathered data from the dns-rpk-why package should be processed.
We don't want to do anything manually except for setting up the config file, 
fixing some cronjobs, as that's fine by us, and input a list with IP addresses of authoritatives. 

Outputting new files, is fine for now! 
'''


class Analyzer:

    def __init__(self, mode, correlation):
        parser = configparser.ConfigParser()
        parser.read("/etc/dns-rpk-why/conf.d/config")

        self.valid_location = parser.get("analyzer", "valid_directory", fallback="Not set")
        self.invalid_location = parser.get("analyzer", "invalid_directory", fallback="Not set")
        self.feed_location = parser.get("analyzer", "feed_directory", fallback="Not set")
        self.output_directory = parser.get("analyzer", "output_directory", fallback="Not set")
        self.filename = parser.get("dnsaal", "output", fallback="Not set")
        self.a_list = parser.get("analyzer", "a_list", fallback="Not set")
        self.aaaa_list = parser.get("analyzer", "aaaa_list", fallback="Not set")
        self.log_file = parser.get("analyzer", "log_file", fallback="analyzer.log")

        self.temp = []
        self.v4 = []
        self.v6 = []
        self.v4_valid = []
        self.v6_valid = []
        self.v4_invalid = []
        self.v6_invalid = []
        self.v4_reference = []
        self.v6_reference = []
        self.v4_no_response = []
        self.v6_no_response = []

        '''
        'normal' = analyze everything from the previous day, such that the script can run as a cronjob
        'today' = analyze everything thus far (only today)
        'DD-MM-YYYY' = custom day to analyze
        '''
        self.mode = mode
        self.correlation = correlation

        self.logger = self._init_logger()
        self.logger.info("DNS-RPK-WHY Analyzer instance created")

        os.system("taskset -p 0xff %d" % os.getpid())

    def _init_logger(self):
        logging.basicConfig(filename=self.log_file, filemode='a+', format="%(levelname)8s | %(asctime)s | %(message)s",
                            datefmt='%Y-%m-%d %H:%M:%S')

        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        stdout_handler = logging.StreamHandler()
        logger.addHandler(stdout_handler)
        return logger

    def _handle_sigterm(self, sig, frame):
        self.logger.warning('SIGTERM received...')

    def get_directories(self):
        return {
            'valid_location': self.valid_location,
            'invalid_location': self.invalid_location,
            'feed_location': self.feed_location
        }

    '''
    Currently we have 3 types of files. 
    Valid and invalid and the list used to feed the sender. 
    These are all 3 seperate locations. So, lets make a config file identfying the directories.
    We have different forms of naming schemes: 
    Always starting with `DD-MM-YYYY_HH_*_authoritative_answers_*.csv
    For now, let's take the input of the dnsaal config. 
    For now we use 2 lists of autoritatives from OPENintel
    The sender takes the file as input, here we read it from the config for now.
    
    takes a filename
    appends all entries of {valid,invalid} per file to a list of v4 or v6
    Input filename must be of .csv
    '''
    def read_file(self, file):
        if self.filename[:-4] in file:
            with open(file, mode='r') as f:
                self.logger.info(f'Reading {file}')
                for row in f:
                    self.sanitize(row.split(',')[2].strip())
        # If the file is from OPENintel, we need to strip, if already processed, data might look different.
        # Future work: add parser and make it work with csv of different columns and addresses in different places.
        elif (self.a_list in file) or (self.aaaa_list in file):
            with open(file, mode='r') as f:
                self.logger.info(f'Reading {file}')
                for row in f:
                    self.sanitize(row.strip())
        else:
            raise ValueError

    def sanitize(self, address):
        try:
            if ipaddress.IPv6Address(address):
                self.v6.append(address)
        except ValueError:
            pass
        try:
            if ipaddress.IPv4Address(address):
                self.v4.append(address)
        except ValueError:
            pass

    def check_mode(self):
        day = ''
        if self.mode == 'normal':
            day = self.get_previous_day()
        elif self.mode == 'today':
            day = self.get_day()
        elif datetime.strptime(self.mode, '%d-%m-%Y') is False:
            raise ValueError
        else:
            day = self.mode
        return day

    def get_len(self, variable):
        if type(variable) is int:
            variable = variable
        elif type(variable) is list:
            variable = len(variable)
        else:
            raise ValueError
        return variable

    '''
    Produce a list of filenames for that day
    In the future a specific timeframe? 
    '''
    def filenames(self, directory):
        parts = []
        os.chdir(directory)
        extension = 'csv'
        all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
        for filename in all_filenames:
            try:
                if str(self.check_mode()) in filename[0:10]:
                    row = {'day': filename[0:10], 'filename': filename}
                    parts.append(row)
            except Exception as e:
                pass
        return parts

    def uniques(self, addresses):
        return list(dict.fromkeys(addresses))

    def get_day(self):
        return datetime.now().strftime('%d-%m-%Y')

    def get_previous_day(self):
        return (datetime.now() - timedelta(days=1)).strftime('%d-%m-%Y')

    def reset_lists(self):
        self.temp = []
        self.v4 = []
        self.v6 = []

    '''
    Takes a list
    writes to file one day combined
    '''
    def write_file(self, addresslist, filename):
        filepath = self.output_directory + filename + '_' + self.check_mode()
        with open(filepath, 'w+') as output:
            output.seek(0)
            for address in addresslist:
                output.write(address + '\n')

    '''
    Format for computed totals:
    Take day + what total to write 
    '''
    def write_to_json(self):
        filepath = self.output_directory + 'statistics.json'
        new_data = {
            str(self.check_mode()): {
                'v4_reference': len(self.v4_reference),
                'v4_valid': len(self.v4_valid),
                'v4_invalid': len(self.v4_invalid),
                'v4_no_response': self.get_len(self.v4_no_response),
                'v6_reference': len(self.v6_reference),
                'v6_valid': len(self.v6_valid),
                'v6_invalid': len(self.v6_invalid),
                'v6_no_response': self.get_len(self.v6_no_response)
            }
        }

        if not os.path.isfile(filepath):
            with open(filepath, 'a+') as x:
                json.dump(new_data, x, indent=4, skipkeys=True)
                x.close()
        elif os.path.exists(filepath):
            with open(filepath, 'r+') as file:
                file_data = json.load(file)
                file_data.update(new_data)
                file.seek(0)
                json.dump(file_data, file, indent=4, skipkeys=True)
                file.close()
        else:
            raise AttributeError

    def analyze_valid(self):
        for file in self.filenames(self.valid_location):
            self.read_file(self.valid_location + '/' + file['filename'])
        self.v4_valid = self.uniques(self.v4)
        #self.write_file(self.v4_valid, 'v4_valid')
        self.v6_valid = self.uniques(self.v6)
        #self.write_file(self.v6_valid, 'v6_valid')
        self.reset_lists()

    def analyze_invalid(self):
        for file in self.filenames(self.invalid_location):
            self.read_file(self.invalid_location + '/' + file['filename'])
        self.v4_invalid = self.uniques(self.v4)
        #self.write_file(self.v4_invalid, 'v4_invalid')
        self.v6_invalid = self.uniques(self.v6)
        #self.write_file(self.v6_invalid, 'v6_invalid')
        self.reset_lists()

    def analyze_reference(self):
        self.read_file(self.feed_location + '/' + self.a_list)
        self.v4_reference = self.uniques(self.v4)
        #self.write_file(self.v4_reference, 'v4_reference')
        self.read_file(self.feed_location + '/' + self.aaaa_list)
        self.v6_reference = self.uniques(self.v6)
        #self.write_file(self.v6_reference, 'v6_reference')
        self.reset_lists()

    def compute_non_responsive_v4(self):
        if self.correlation:
            for address in self.v4_reference:
                if address not in self.v4_valid and address not in self.v4_invalid:
                    self.v4_no_response.append(address)
        else:
            self.v4_no_response = len(self.v4_reference) - (len(self.v4_valid) + len(self.v4_invalid))

    def compute_non_responsive_v6(self):
        if self.correlation:
            for address in self.v6_reference:
                if address not in self.v6_valid and address not in self.v6_invalid:
                    self.v6_no_response.append(address)
        else:
            self.v6_no_response = len(self.v6_reference) - (len(self.v6_valid) + len(self.v6_invalid))

    def compute_non_responsive(self):
        self.logger.info(f'Available cores {multiprocessing.cpu_count()}')
        compute_v4 = multiprocessing.Process(target=self.compute_non_responsive_v4())
        compute_v6 = multiprocessing.Process(target=self.compute_non_responsive_v6())
        self.logger.info(f'Starting calculating non responsive IPv4 addresses')
        compute_v4.start()
        self.logger.info(f'Starting calculating non responsive IPv6 addresses')
        compute_v6.start()
        compute_v4.join()
        compute_v6.join()

    def analyze_day(self):
        pass

    def run(self):
        self.logger.info(f'Begin analyzing {self.check_mode()}')
        self.analyze_reference()
        self.logger.info(f'Reference done')
        self.analyze_valid()
        self.logger.info(f'Valid done')
        self.analyze_invalid()
        self.logger.info(f'Invalid done')
        self.compute_non_responsive()
        #self.compute_non_responsive()
        self.logger.info(f'Non responsive done')
        self.write_to_json()
        self.logger.info(f'Json done, bye')
        if self.correlation:
            options = {
                'v4_valid': self.v4_valid,
                'v4_invalid': self.v4_invalid,
                'v4_no_response': self.v4_no_response,
                'v6_valid': self.v6_valid,
                'v6_invalid': self.v6_invalid,
                'v6_no_response': self.v6_no_response
            }
            for option in options:
                self.write_file(options[option], option)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='DNS-RPK-WHY Analyzer',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("mode",
                        help="Type of operation: normal, today or date in form DD-MM-YYYY",
                        type=str)
    parser.add_argument("correlation",
                        help="Correlation: write addresses to separate files for correlation",
                        const=False,
                        nargs="?")
    analyzer = Analyzer(parser.parse_args().mode, parser.parse_args().correlation)
    analyzer.run()
