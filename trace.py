import mtrpacket, asyncio


class Trace:

    def __init__(self, dst_address, src_address):
        self.dst_address = dst_address
        self.src_address = src_address

    async def probe(self):
        async with mtrpacket.MtrPacket() as mtr:
            return await mtr.probe(
                host=self.dst_address,
                protocol='udp',
                local_ip=self.src_address
            )


if __name__ == '__main__':
    trace = Trace()
    print(trace.probe())
