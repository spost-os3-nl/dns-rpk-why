# Measure RPKI adoption of authoritative name servers

This project is mostly written in Python3.

### Installation 

* Run `pip3 install -r requirements.txt`
* Run `install.sh`
* Run `systemctl daemon-reload`
* Run `systemctl enable dnsaal`
* Run `systemctl restart dnsaal`
* Run `python3 sender.py normal`

Where `normal` means send queries from a 2 column csv, where authoritative IPs are in the second column.

### Gotchas

* ` usermod -a -G gitlab-runner dnsaal` 

Because, no time to fix at this point...

* `cp copy.sh /etc/cron.hourly/copy`
* `chmod +x /etc/cron.hourly/copy `

Because, cron and rsync...