#!/bin/bash

set -x

if [[ $(hostname -s) = aws* ]];
then
  rsync -e "ssh -i /root/.ssh/dns-rpk-why-scp" /home/gitlab-runner/dns-rpk-why/*_authoritative_answers.csv bobby@145.100.107.206:/dnsdata/valid
else
  rsync -e "ssh -i /root/.ssh/dns-rpk-why-scp" /home/gitlab-runner/dns-rpk-why/*_authoritative_answers.csv bobby@145.100.107.206:/dnsdata/invalid
fi
