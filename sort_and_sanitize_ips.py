#!/usr/bin/python3

import ipaddress, csv, os, argparse, random
import random


class Sorter:

    def __init__(self, infile, outfile, sorting):
        self.cwd = os.getcwd()
        self.new_csv = "ordered_ipv6.csv"
        self.input = infile
        self.output = outfile
        self.sorting = sorting
        self.ips = []

    def write_file(self):
        with open(self.cwd + '/' + self.output, mode='w+') as file:
            writer = csv.writer(file)
            for address in self.ips:
                if self.sanitize(address):
                    writer.writerow({address})

    def read_file(self):
        with open(self.cwd + '/' + self.input, mode='r') as file:
            for row in file:
                self.ips.append((row.split(',')[1].strip()))

    def sort_addresses(self):
        self.ips = sorted(self.ips, key=ipaddress.ip_address)

    def sanitize(self, address):
        try:
            if ipaddress.IPv6Address(address).is_global:
                return address
        except ValueError:
            pass
        try:
            if ipaddress.IPv4Address(address).is_global:
                return address
        except ValueError:
            pass

    def remove_duplicates(self):
        self.ips = list(dict.fromkeys(self.ips))

    def randomize(self):
        random.shuffle(self.ips)

    def main(self):
        self.read_file()
        self.remove_duplicates()
        if self.sorting == "sorted":
            self.sort_addresses()
        if self.sorting == "random":
            self.randomize()
        self.write_file()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='Sort IP addresses',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("input",
                        help='input file',
                        type=str)
    parser.add_argument("output",
                        help='output file',
                        type=str)
    parser.add_argument("sorting",
                        help='sorting type',
                        choices=['sorted', 'random'],
                        type=str)
    sorter = Sorter(parser.parse_args().input, parser.parse_args().output, parser.parse_args().sorting)
    sorter.main()
