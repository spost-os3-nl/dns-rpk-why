#!/usr/bin/python3

import json, configparser, datetime, argparse
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

'''
This class needs Ubuntu packages: pdflatex, texlive-science
MacOS: basictex, texlive
'''
class Grapher:

    def __init__(self):
        parser = configparser.ConfigParser()
        parser.read("/etc/dns-rpk-why/conf.d/config")

        self.output_directory = parser.get("analyzer", "output_directory", fallback="Not set")
        self.json_data = None
        self.domain_data = []
        self.show = True
        self.save = True
        self.latex = False
        self.darkblue = '#001cbc'
        self.blue = '#1f50ef'
        self.lightblue = '#2da0e2'
        self.red = '#ea2324'
        self.green = '#60a917'
        self.grey = '#808080'
        self.black = '#020202'
        self.alphagreen = '#a1cd77'
        self.alphagrey = '#b2b2b2'
        self.alphablue = '#7b93f3'
        self.googlegrey = '#343434'

        if self.latex:
            mpl.use('pgf')
            mpl.rcParams.update({
                'pgf.texsystem': 'pdflatex',
                'font.family': 'Times New Roman',
                'font.size': 8,
                'pgf.rcfonts': False,
            })
        else:
            mpl.rcParams.update({
                'font.family': 'Exo 2',
                'figure.titlesize': 36,
                'axes.titlesize': 24,
                'axes.labelsize': 24,
                'xtick.labelsize': 18,
                'ytick.labelsize': 18,
                'legend.fontsize': 18,
                'text.color': self.googlegrey,
                'figure.titleweight': 'bold',
                'figure.figsize': '14,6'

            })
        from matplotlib import font_manager
        font_dirs = ['/Users/sanderpost/Library/Fonts']
        font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
        for font_file in font_files:
            print(font_file)
            font_manager.fontManager.addfont(font_file)

    def what_to_do(self, plot, fig):
        plt.tight_layout()
        if self.latex and self.save:
            plt.savefig(f'{self.output_directory}/{fig._suptitle.get_text()}.pgf',
                        bbox_inches='tight', pad_inches=0)
        if self.save:
            plt.savefig(f'{self.output_directory}/{fig._suptitle.get_text()}.png',
                        bbox_inches='tight', pad_inches=0, dpi=600)
        if self.show:
            plot.show()

    # Note: I don't remember who wrote the set_size function, but it's from Stackoverflow.
    def set_size(self, width_pt, fraction=1, subplots=(1, 1)):
        """Set figure dimensions to sit nicely in our document.

        Parameters
        ----------
        width_pt: float
                Document width in points
        fraction: float, optional
                Fraction of the width which you wish the figure to occupy
        subplots: array-like, optional
                The number of rows and columns of subplots.
        Returns
        -------
        fig_dim: tuple
                Dimensions of figure in inches
        """
        # Width of figure (in pts)
        fig_width_pt = width_pt * fraction
        # Convert from pt to inches
        inches_per_pt = 1 / 72.27

        # Golden ratio to set aesthetic figure height
        golden_ratio = (5 ** .5 - 1) / 2

        # Figure width in inches
        fig_width_in = fig_width_pt * inches_per_pt
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

        if self.latex:
            return (fig_width_in, fig_height_in)
        else:
            return (20, 8)

    '''
    Input is a timeframe. 
    begin_date = DD-MM-YYYY 
    end_date = dD-MM-YYYY
    returns dict of datetimes in that frame
    '''
    def get_timeframe(self, begin_date, end_date):
        if begin_date == end_date:
            return [begin_date]
        end_date = datetime.datetime.strptime(end_date, '%d-%m-%Y')
        begin_date = datetime.datetime.strptime(begin_date, '%d-%m-%Y')
        delta = end_date - begin_date
        days = [begin_date + datetime.timedelta(days=i) for i in range(delta.days + 1)]
        return days

    def check_latex(self):
        pass

    def get_size(self):
        if self.latex:
            return 234
        else:
            return 1080

    def read_json(self):
        filepath = self.output_directory + 'statistics.json'
        with open(filepath, 'r') as file:
            json_data = json.load(file)
        self.json_data = json_data

    def read_domain_stats(self):
        filepath = self.output_directory + 'domains_counted'
        with open(filepath, 'r') as file:
            for line in file:
                row = {'title': line.split(',')[0].strip(), 'total': line.split(',')[1].strip()}
                self.domain_data.append(row)

    def testing(self):
        for element in self.get_timeframe('24-06-2022', '27-06-2022'):
            print(element.strftime('%d-%m-%Y'))

    def per_day_plot(self, day):
        try:
            total = [self.json_data[day]['v4_reference'], self.json_data[day]['v6_reference']]
            valid = [self.json_data[day]['v4_valid'], self.json_data[day]['v6_valid']]
            invalid = [self.json_data[day]['v4_invalid'], self.json_data[day]['v6_invalid']]
        except KeyError:
            pass
        n = 2
        ind = np.arange(n)
        width = 0.9
        mpl.style.use('seaborn')
        fig = plt.figure(figsize=self.set_size(self.get_size()))
        fig.suptitle(f'Overview of results on {day}')
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        ax.bar(ind, total, width, fill=True, color=self.grey, edgecolor='black')
        ax.bar(ind, valid, width, color=self.green, edgecolor='black')
        ax.bar(ind, invalid, width, bottom=valid, color=self.blue, edgecolor='black')

        ax.set_ylabel('Total queries sent', visible=True)
        #ax.set_title(f'Overview of results per autoritative on {day}', visible=True)
        ax.set_xticks(ind, ('IPv4', 'IPv6'), visible=True)
        ax.legend(labels=['No response', 'RPKI validated', 'RPKI unvalidated'])

        self.what_to_do(plt, fig)

    def addpercentages(self, plot, x, y, percentage):
        for i in range(len(x)):
            plot.text(i, y[i], f'{round(percentage[i]//2)}%', ha='center', fontsize=17,)

    def plot_roa(self, list_of_days):
        filepath = self.output_directory + 'roa.json'
        with open(filepath, 'r') as file:
            json_data = json.load(file)
        json_data = json_data

        del list_of_days[4:6]
        x = {
            "v4_reference": [],
            "v6_reference": [],
            'v4_invalid_with_roa': [],
            'v6_invalid_with_roa': [],
            'v4_valid_with_roa': [],
            'v6_valid_with_roa': []
        }
        v4_valid_percentage = []
        v6_valid_percentage = []
        v6_invalid_percentage = []
        v4_invalid_percentage = []
        v4_total = 0
        v6_total = 0

        # Needs a list per type of measurement containing all days of the timedelta
        n = len(list_of_days)
        ind = np.arange(n)
        total = [100 for y in ind]
        width = 0.9
        mpl.style.use('ggplot')
        # add all measurements to a dict for easy plotting
        for element in list_of_days:
            try:
                for measurement in json_data[element.strftime('%d-%m-%Y')]:
                    #print(measurement)
                    x[measurement].append(json_data[element.strftime('%d-%m-%Y')][measurement])
                    if measurement == 'v4_reference':
                        v4_total = json_data[element.strftime('%d-%m-%Y')][measurement]
                    if measurement == 'v6_reference':
                        v6_total = json_data[element.strftime('%d-%m-%Y')][measurement]
                    if measurement == 'v4_valid_with_roa':
                        #print(type(json_data[element.strftime('%d-%m-%Y')][measurement]))
                        v4_valid_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')][measurement], v4_total))
                    if measurement == 'v4_invalid_with_roa':
                        #print(json_data[element.strftime('%d-%m-%Y')][measurement])
                        v4_invalid_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')][measurement], v4_total))
                    if measurement == 'v6_valid_with_roa':
                        #print(json_data[element.strftime('%d-%m-%Y')][measurement])
                        v6_valid_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')][measurement], v6_total))
                    if measurement == 'v6_invalid_with_roa':
                        #print(v6_total)
                        #print((json_data[element.strftime('%d-%m-%Y')][measurement]/v6_total))
                        v6_invalid_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')][measurement], v6_total))
                        #v6_invalid_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')][measurement], v6_total))
            except KeyError:
                # Day non existent, in the future raise KeyError anyway :)
                pass

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.set_size(self.get_size()))
        fig.suptitle(f'General state of addresses covered by a ROA')
        # ax1 = fig.add_axes([0,0,1,1])
        #ax1.bar(ind, x['v4_reference'], width, fill=True, color=self.grey, edgecolor='black'),
        ax1.bar(ind, x['v4_valid_with_roa'], width, color=self.green, edgecolor='black')
        ax1.bar(ind, x['v4_invalid_with_roa'], width, bottom=x['v4_valid_with_roa'], color=self.blue, edgecolor='black'),
        for bar in ax1.patches:
            ax1.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v4_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )
        #ax2.bar(ind, x['v6_reference'], width, fill=True, color=self.grey, edgecolor='black')
        ax2.bar(ind, x['v6_valid_with_roa'], width, color=self.green, edgecolor='black')
        ax2.bar(ind, x['v6_invalid_with_roa'], width, bottom=x['v6_valid_with_roa'], color=self.blue, edgecolor='black')
        for bar in ax2.patches:
            ax2.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v6_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )

        ax1.set_ylabel('Total number of authoritative name servers', fontsize=18)
        ax1.set_title('IPv4', fontsize=18)
        ax2.set_title('IPv6', fontsize=18)
        ax1.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.yaxis.tick_right()
        # fig.supxlabel('days')
        fig.legend(bbox_to_anchor=(0.5, -0.025), ncol=3, loc='center',
                   labels=['Covered, arrived at valid collector', 'Covered, arrived on invalid collector'])

        self.what_to_do(plt, fig)

    def graph_subplots(self, list_of_days):
        del list_of_days[4:6]
        x = {
            'v4_reference': [],
            'v4_valid': [],
            'v4_invalid': [],
            'v4_no_response': [],
            'v6_reference': [],
            'v6_valid': [],
            'v6_invalid': [],
            'v6_no_response': []
        }
        # Needs a list per type of measurement containing all days of the timedelta
        n = len(list_of_days)
        ind = np.arange(n)
        width = 0.9
        mpl.style.use('ggplot')

        # add all measurements to a dict for easy plotting
        for element in list_of_days:
            print(element.strftime('%d-%m-%Y'))
            print(self.json_data)
            try:
                for measurement in self.json_data[element.strftime('%d-%m-%Y')]:
                    print(measurement)
                    x[measurement].append(self.json_data[element.strftime('%d-%m-%Y')][measurement])
            except KeyError:
                # Day non existent, in the future raise KeyError anyway :)
                pass
        print(x)
        v4_total = x['v4_reference'][0]
        v6_total = x['v6_reference'][0]

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.set_size(self.get_size()))
        fig.suptitle(f'General overview of results per day')
        # ax1 = fig.add_axes([0,0,1,1])
        #ax1.bar(ind, x['v4_reference'], width, fill=True, color=self.grey, edgecolor='black')
        ax1.bar(ind, x['v4_valid'], width, color=self.green, edgecolor='black')
        ax1.bar(ind, x['v4_invalid'], width, bottom=x['v4_valid'], color=self.blue, edgecolor='black')
        for bar in ax1.patches:
            ax1.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v4_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )
        #ax2.bar(ind, x['v6_reference'], width, fill=True, color=self.grey, edgecolor='black')
        ax2.bar(ind, x['v6_valid'], width, color=self.green, edgecolor='black')
        ax2.bar(ind, x['v6_invalid'], width, bottom=x['v6_valid'], color=self.blue, edgecolor='black')
        for bar in ax2.patches:
            ax2.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v6_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )

        ax1.set_ylabel('Total number of authoritative nameservers', fontsize=18)
        ax1.set_title('IPv4', fontsize=18)
        ax2.set_title('IPv6', fontsize=18)
        ax1.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.yaxis.tick_right()
        # fig.supxlabel('days')
        fig.legend(bbox_to_anchor=(0.5, -0.025), ncol=3, loc='center',
                   labels=['Arrived at valid collector', 'Arrived at invalid collector'])

        self.what_to_do(plt, fig)

    def daily_percentage(self, day):
        #v4_no_response_perc = str(round((((sum(v4_no_response) / len(v4_no_response)) / 791113) * 100), 1)) + '%'
        #v4_invalid_perc = str(round((((sum(v4_invalid) / len(v4_invalid)) / 791113) * 100), 1)) + '%'
        #v4_valid_perc = str(round((((sum(v4_valid) / len(v4_valid)) / 791113) * 100), 1)) + '%'
        #v6_no_response_perc = str(round((((sum(v6_no_response) / len(v6_no_response)) / 79701) * 100), 1)) + '%'
        #v6_invalid_perc = str(round((((sum(v6_invalid) / len(v6_invalid)) / 79701) * 100), 1)) + '%'
        #v6_valid_perc = str(round((((sum(v6_valid) / len(v6_valid)) / 79701) * 100), 1)) + '%'
        pass

    def doughnut(self, day):
        fig, ax = plt.subplots(figsize=self.set_size(self.get_size()))
        ax.axis('equal')
        width = 0.4

        # cm = plt.get_cmap("tab20c")
        fig.suptitle(f'Overview of results on {day}', x=0.71, y=0.87)

        #lin = [v4_no_response_perc, v4_valid_perc, v4_invalid_perc]
        #lout = [v6_no_response_perc, v6_valid_perc, v6_invalid_perc]


        pie, _ = ax.pie(
            [self.json_data[day]['v4_valid'], self.json_data[day]['v4_invalid'], self.json_data[day]['v4_no_response']],
            radius=1,
            colors=[self.grey, self.green, self.blue],
            labeldistance=0.64,
            labels=['x', 'y', 'z'],
            rotatelabels=True,
            textprops=dict(rotation_mode='anchor', va='center', color='white')
        )
        plt.setp(pie, width=width, edgecolor='white')
        pie2, _ = ax.pie([self.json_data[day]['v6_valid'],
                          self.json_data[day]['v6_invalid'],
                          self.json_data[day]['v6_no_response']],
            radius=1 - width,
            colors=[self.alphagrey, self.alphagreen, self.alphablue],
            labeldistance=0.43,
            labels=['x', 'y', 'z'],
            rotatelabels=True,
            textprops=dict(rotation_mode='anchor', va='center', color='black')
        )
        plt.setp(pie2, width=width, edgecolor='white')

        # fig.legend(bbox_to_anchor=(0.95, 0.5), loc='center left', labels=['IPv4 No response', 'IPv4 RPKI validated', 'IPv4 RPKI unvalidated', 'IPv6 No response', 'IPv6 RPKI validated', 'IPv6 RPKI unvalidated'])

        # plt.tight_layout()
        # plt.subplots_adjust(left=0.2, wspace=0.2)

        self.what_to_do(plt, fig)

    def percentage(self, value, total):
        return (int(value)/int(total))*100

    def addlabels(self, plot, x, y, percentage):
        for i in range(len(x)):
            plot.text(i, 0, f'{round(percentage[i],2)}%', ha='center', fontsize=16, va='top',
                      bbox=dict(facecolor='white', alpha=.9))

    def plot_overlap(self, list_of_days):
        del list_of_days[4:6]
        filepath = self.output_directory + 'overlap.json'
        with open(filepath, 'r') as file:
            json_data = json.load(file)
        x = json_data
        v4_dups = []
        v4_percentage = []
        v6_dups = []
        v6_percentage = []

        for element in list_of_days:
            try:
                v4_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')]['v4'],
                                      json_data[element.strftime('%d-%m-%Y')]['v4_reference']))
                v6_percentage.append(self.percentage(json_data[element.strftime('%d-%m-%Y')]['v6'],
                                      json_data[element.strftime('%d-%m-%Y')]['v6_reference']))
                for measurement in json_data[element.strftime('%d-%m-%Y')]:
                    if measurement == 'v4':
                        v4_dups.append(json_data[element.strftime('%d-%m-%Y')][measurement])
                    if measurement == 'v6':
                        v6_dups.append(json_data[element.strftime('%d-%m-%Y')][measurement])
            except KeyError:
                # Day non existent, in the future raise KeyError anyway :)
                pass

        n = len(list_of_days)
        ind = np.arange(n)
        width = 0.9
        mpl.style.use('ggplot')
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.set_size(self.get_size()))
        fig.suptitle(f'Amount of duplicates arrived at both valid and invalid collectors')
        # ax1 = fig.add_axes([0,0,1,1])
        ax1.bar(ind, v4_dups, width, color=self.blue, edgecolor='black')
        ax2.bar(ind, v6_dups, width, color=self.blue, edgecolor='black')

        ax1.set_ylabel('Amount of duplicates', fontsize=18)
        ax1.set_title('IPv4', fontsize=18)
        ax2.set_title('IPv6', fontsize=18)
        ax1.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax1.tick_params(axis='x', pad=15)
        ax1.ticklabel_format(axis='y', style='plain')
        self.addlabels(ax1, ind, v4_dups, v4_percentage)
        ax2.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.tick_params(axis='x', pad=15)
        ax2.ticklabel_format(axis='y', style='plain')
        self.addlabels(ax2, ind, v6_dups, v6_percentage)
        #ax2.bar_label(v6_dups, label_type='center')
        # ax2.yaxis.tick_right()
        #fig.supxlabel('days')
        #fig.legend(bbox_to_anchor=(0.5, -0.025), ncol=3, loc='center',
        #           labels=['No response', 'RPKI validated', 'RPKI unvalidated'])
        ax2.yaxis.tick_right()
        self.what_to_do(plt, fig)

    def plot_domains(self, list_of_days):
        del list_of_days[4:6]
        x = {
            'v4_no_response': [],
            'v4_valid': [],
            'v4_invalid': [],
            'v4_total': [],
            'v6_no_response': [],
            'v6_valid': [],
            'v6_invalid': [],
            'v6_total': []
        }
        p = {
            'v4_no_response': [],
            'v4_valid': [],
            'v4_invalid': [],
            'v4_total': [],
            'v6_no_response': [],
            'v6_valid': [],
            'v6_invalid': [],
            'v6_total': []
        }

        n = len(list_of_days)
        ind = np.arange(n)
        v4_total = 0
        v6_total = 0
        for day in ind:
            for element in self.domain_data:
                if list_of_days[day].strftime('%d-%m') in element['title']:
                    if 'v4-total' in element['title']:
                        v4_total = int(element['total'])
                        x['v4_total'].append(int(element['total']))
                        p['v4_total'].append(self.percentage(element['total'], v4_total))
                    if 'v4-valid' in element['title']:
                        p['v4_valid'].append(self.percentage(element['total'], v4_total))
                        #x['v4_valid'].append(int(element['total']))
                        x['v4_valid'].append(int(element['total']))
                    if 'v4-invalid' in element['title']:
                        p['v4_invalid'].append(self.percentage(element['total'], v4_total))
                        #x['v4_invalid'].append(int(element['total']))
                        x['v4_invalid'].append(int(element['total']))
                    if 'v4-no-response' in element['title']:
                        p['v4_no_response'].append(self.percentage(element['total'], v4_total))
                        #x['v4_no_response'].append(int(element['total']))
                        x['v4_no_response'].append(int(element['total']))
                    if 'v6-total' in element['title']:
                        v6_total = int(element['total'])
                        p['v6_total'].append(self.percentage(element['total'], v6_total))
                        x['v6_total'].append(int(element['total']))
                    if 'v6-valid' in element['title']:
                        p['v6_valid'].append(self.percentage(element['total'], v6_total))
                        #x['v6_valid'].append(int(element['total']))
                        x['v6_valid'].append(int(element['total']))
                    if 'v6-invalid' in element['title']:
                        p['v6_invalid'].append(self.percentage(element['total'], v6_total))
                        #x['v6_invalid'].append(int(element['total']))
                        x['v6_invalid'].append(int(element['total']))
                    if 'v6-no-response' in element['title']:
                        p['v6_no_response'].append(self.percentage(element['total'], v6_total))
                        #x['v6_no_response'].append(int(element['total']))
                        x['v6_no_response'].append(int(element['total']))
        print(p)

        width = 0.9
        mpl.style.use('ggplot')
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.set_size(self.get_size()))
        fig.suptitle(f'RPKI state of domains served by authoritatives per day')
        # ax1 = fig.add_axes([0,0,1,1])
        #ax1.bar(ind, x['v4_total'], width, color=self.grey, edgecolor='black')
        ax1.bar(ind, x['v4_valid'], width, color=self.green, edgecolor='black')
        ax1.bar(ind, x['v4_invalid'], width, bottom=x['v4_valid'], color=self.blue, edgecolor='black')
        for bar in ax1.patches:
            ax1.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v4_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )
        #ax1.bar(ind, x['v4_no_response'], width, bottom=x['v4_valid']+x['v4_invalid'], color=self.grey, edgecolor='black')
        #ax2.bar(ind, x['v6_total'], width, color=self.grey, edgecolor='black')
        ax2.bar(ind, x['v6_valid'], width, color=self.green, edgecolor='black')
        ax2.bar(ind, x['v6_invalid'], width, bottom=x['v6_valid'], color=self.blue, edgecolor='black')
        for bar in ax2.patches:
            ax2.text(
                bar.get_x() + bar.get_width() / 2,
                (bar.get_height() / 2) + bar.get_y(),
                f'{round((bar.get_height()/v6_total*100), 2)}%',
                ha='center',
                color='w',
                size=14
            )

        ax1.set_ylabel('Total number of domains served by authoritatives', fontsize=18)
        ax1.set_title('IPv4', fontsize=18)
        ax2.set_title('IPv6', fontsize=18)
        ax1.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax1.ticklabel_format(axis='y', style='plain')
        ax2.set_xticks(ind, labels=[day.strftime('%d-%m-%Y') for day in list_of_days], rotation=45)
        ax2.ticklabel_format(axis='y', style='plain')
        ax2.yaxis.tick_right()
        # fig.supxlabel('days')
        fig.legend(bbox_to_anchor=(0.5, -0.025), ncol=3, loc='center',
                   labels=['Arrived at valid collector', 'Arrived at invalid collector'])
        print(plt.style.available)
        self.what_to_do(plt, fig)




if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='DNS-RPK-WHY Grapher',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("--sub-plots",
                        help="Get combined subplots, multiple days, takes begin date and end date in DD-MM-YYYY format",
                        nargs=2,
                        metavar=('begin_day', 'end_day'),
                        default=(datetime.datetime.now().strftime('%d-%m-%Y'), datetime.datetime.now().strftime('%d-%m-%Y')),
                        type=str)
    parser.add_argument("--plot-domains",
                        help="Lekker bier drinken",
                        nargs=2,
                        metavar=('begin_day', 'end_day'),
                        default=(datetime.datetime.now().strftime('%d-%m-%Y'), datetime.datetime.now().strftime('%d-%m-%Y')),
                        type=str)
    parser.add_argument("--plot-overlap",
                        help="Lekker bier drinken",
                        nargs=2,
                        metavar=('begin_day', 'end_day'),
                        default=(datetime.datetime.now().strftime('%d-%m-%Y'), datetime.datetime.now().strftime('%d-%m-%Y')),
                        type=str)
    parser.add_argument("--plot-roa",
                        help="Lekker bier drinken",
                        nargs=2,
                        metavar=('begin_day', 'end_day'),
                        default=(datetime.datetime.now().strftime('%d-%m-%Y'), datetime.datetime.now().strftime('%d-%m-%Y')),
                        type=str)
    parser.add_argument("--per-day",
                        help="Per day plot, takes begin date and end date in DD-MM-YYYY format",
                        metavar='day',
                        default=datetime.datetime.now().strftime('%d-%m-%Y'),
                        type=str)
    parser.add_argument("--latex",
                        help="Export to Latex, no gui output",
                        metavar='latex',
                        default=False
                        )
    args = vars(parser.parse_args())
    print(args)
    grapher = Grapher()
    grapher.read_json()
    grapher.read_domain_stats()
    grapher.latex = args['latex']
    grapher.graph_subplots(grapher.get_timeframe(args['sub_plots'][0], args['sub_plots'][1]))
    grapher.plot_domains(grapher.get_timeframe(args['plot_domains'][0], args['plot_domains'][1]))
    #grapher.per_day_plot(args['per_day']),
    grapher.plot_overlap(grapher.get_timeframe(args['plot_overlap'][0], args['plot_overlap'][1]))
    grapher.plot_roa(grapher.get_timeframe(args['plot_roa'][0], args['plot_roa'][1]))