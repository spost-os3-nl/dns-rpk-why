#!/usr/bin/python3

import configparser
import datetime
import ipaddress
import os
import glob
import json
import argparse
import multiprocessing
import asyncio

'''
Reading the Analyzer output: {v4,v6}_invalid_{DD-MM-YYYY}
Reading the Validated ROA Payloads in CSV format from Routinator

Comparing autoritative responses collected on the invalid probe with the VRPs
On match: 
    - Prefix is signed but no ROV with dropping invalids.
    - No ROV on the AS_PATH

Comparing autoritative responses collected on the valid probe with the VRPs
On match: 
    - Prefix is signed and does ROV
No match: 
    - Question mark? Invalid probes AS does not do ROV with dropping invalids.
'''


class VRPCheck:

    def __init__(self, mode):
        parser = configparser.ConfigParser()
        parser.read("/etc/dns-rpk-why/conf.d/config")

        self.output_directory = parser.get("analyzer", "output_directory", fallback="/dnsdata/analyzer")
        self.vrp_location = parser.get("vrp_check", "file", fallback="/dnsdata/rpki/vrps")
        self.v4_vrps = []
        self.v6_vrps = []
        self.v4_valid = []
        self.v6_valid = []
        self.v4_invalid = []
        self.v6_invalid = []
        self.v4_invalid_result = []
        self.v6_invalid_result = []
        self.v4_valid_result = []
        self.v6_valid_result = []
        self.mode = mode
        self.directory = parser.get("analyzer", "output_directory", fallback="Not set")
        self.morefiles = True
        #os.system("taskset -p 0xff %d" % os.getpid())

    def get_day(self):
        return datetime.datetime.now().strftime('%d-%m-%Y')

    def get_previous_day(self):
        return (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%d-%m-%Y')

    def check_mode(self):
        day = ''
        if self.mode == 'normal':
            day = self.get_previous_day()
        elif self.mode == 'today':
            day = self.get_day()
        elif datetime.datetime.strptime(self.mode, '%d-%m-%Y') is False:
            raise ValueError
        else:
            day = self.mode
        return day

    def filenames(self, directory):
        parts = []
        os.chdir(directory)
        all_filenames = [i for i in glob.glob('*')]
        for filename in all_filenames:
            try:
                if str(self.check_mode()) in filename:
                    parts.append(filename)
            except Exception as e:
                pass
        return parts

    def read_vrp(self):
        with open(self.vrp_location, 'r') as vrps:
            for vrp in vrps:
                vrp = vrp.split(',')[1].strip()
                try:
                    if ipaddress.IPv6Network(vrp):
                        self.v6_vrps.append(ipaddress.IPv6Network(vrp))
                except ValueError:
                    pass
                try:
                    if ipaddress.IPv4Network(vrp):
                        self.v4_vrps.append(ipaddress.IPv4Network(vrp))
                except ValueError:
                    pass

    def read_analyzed_data(self, file):
        with open(file, 'r') as addresses:
            for address in addresses:
                address = address.strip()
                try:
                    if ipaddress.IPv6Address(address):
                        if 'v6_valid' in file:
                            self.v6_valid.append(ipaddress.IPv6Address(address))
                        elif 'v6_invalid' in file:
                            self.v6_invalid.append(ipaddress.IPv6Address(address))
                        else:
                            pass
                except ValueError:
                    pass
                try:
                    if ipaddress.IPv4Address(address):
                        if 'v4_valid' in file:
                            self.v4_valid.append(ipaddress.IPv4Address(address))
                        elif 'v4_invalid' in file:
                            self.v4_invalid.append(ipaddress.IPv4Address(address))
                        else:
                            pass
                except ValueError:
                    pass

    def write_file(self, addresslist, filename):
        filepath = self.output_directory + filename + '_' + self.check_mode()
        with open(filepath, 'w+') as output:
            output.seek(0)
            for address in addresslist:
                output.write(str(address) + '\n')

    # We can make this much more efficient. As long is it does the job...
    def correlate_invalid_v4(self):
        for address in self.v4_invalid:
            for vrp in self.v4_vrps:
                if address in vrp:
                    self.v4_invalid_result.append(str(address))

    def correlate_invalid_v6(self):
        for address in self.v6_invalid:
            for vrp in self.v6_vrps:
                if address in vrp:
                    self.v6_invalid_result.append(str(address))

    def correlate_valid_v4(self):
        for address in self.v4_valid:
            for vrp in self.v4_vrps:
                if address in vrp:
                    self.v4_valid_result.append(str(address))

    def correlate_valid_v6(self):
        for address in self.v6_valid:
            for vrp in self.v6_vrps:
                if address in vrp:
                    self.v6_valid_result.append(str(address))

    def write_to_json(self):
        filepath = self.output_directory + 'roa_statistics.json'
        new_data = {
            str(self.check_mode()): {
                'v4_invalid_with_roa': len(self.v4_invalid_result),
                'v6_invalid_with_roa': len(self.v6_invalid_result),
                'v4_valid_with_roa': len(self.v4_valid_result),
                'v6_valid_with_roa': len(self.v6_valid_result)
            }
        }

        if not os.path.isfile(filepath):
            with open(filepath, 'a+') as x:
                json.dump(new_data, x, indent=4, skipkeys=True)
                x.close()
        elif os.path.exists(filepath):
            with open(filepath, 'r+') as file:
                file_data = json.load(file)
                file_data.update(new_data)
                file.seek(0)
                json.dump(file_data, file, indent=4, skipkeys=True)
                file.close()
        else:
            raise AttributeError

    def run(self):
        self.read_vrp()
        for file in self.filenames(self.directory):
            if self.check_mode() and 'v4_invalid' in file:
                self.read_analyzed_data(f'{self.directory}/{file}')
                multiprocessing.Process(target=self.correlate_invalid_v4()).start()
            if self.check_mode() and 'v6_invalid' in file:
                self.read_analyzed_data(f'{self.directory}/{file}')
                multiprocessing.Process(target=self.correlate_invalid_v6()).start()
            if self.check_mode() and 'v4_valid' in file:
                self.read_analyzed_data(f'{self.directory}/{file}')
                multiprocessing.Process(target=self.correlate_valid_v4()).start()
            if self.check_mode() and 'v6_valid' in file:
                self.read_analyzed_data(f'{self.directory}/{file}')
                multiprocessing.Process(target=self.correlate_valid_v6()).start()
        self.write_to_json()
        if self.morefiles:
            options = {
                'v4_valid_with_roa': self.v4_valid,
                'v4_invalid_with_roa': self.v4_invalid,
                'v6_valid_with_roa': self.v6_valid,
                'v6_invalid_with_roa': self.v6_invalid
            }
            for option in options:
                self.write_file(options[option], option)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='DNS-RPK-WHY Analyzer',
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("mode",
                        help="Type of operation: normal, today or date in form DD-MM-YYYY",
                        type=str)
    vrpcheck = VRPCheck(parser.parse_args().mode)
    vrpcheck.run()
